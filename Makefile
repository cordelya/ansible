help:
	@echo "Here's what you can do:"
	@echo "update-pis	: runs ansible update-upgrade playbook"
	@echo "nc-update	: runs apt-get update && apt-list --upgradable on nextcloud"
	@echo "nc-upgrade	: runs apt-get upgrade -y on nextcloud"
	@echo "wp-update	: runs apt-get update && apt-list --upgradable on wordpress"
	@echo "wp-upgrade	: runs apt-get upgrade -y on wordpress"
	@echo "retropie-update	: sorry, pal, you gotta do this one manually"

update-pis:
	ansible-playbook /home/ubuntu/ansible/update-upgrade.yaml

nc-update:
	ssh nextcloud "sudo apt-get update && apt list --upgradable"

nc-upgrade:
	ssh nextcloud sudo apt-get upgrade -y

wp-update:
	ssh bitnami "sudo apt-get update && apt list --upgradable"

wp-upgrade:
	ssh bitnami sudo apt-get upgrade -y
